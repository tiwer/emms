/*
Navicat MariaDB Data Transfer

Source Server         : remote
Source Server Version : 50556
Source Host           : localhost:3306
Source Database       : materialmanager

Target Server Type    : MariaDB
Target Server Version : 50556
File Encoding         : 65001

Date: 2018-02-03 11:55:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_03_01_141819_create_matertial_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_03_02_005930_create_usingRecord_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_03_02_012858_create_treeTrunk_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_03_02_014903_create_company_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_03_02_022527_create_config_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_03_14_033629_create_apply_records_table', '2');
INSERT INTO `migrations` VALUES ('9', '2017_03_14_072417_create_messages_table', '3');
INSERT INTO `migrations` VALUES ('10', '2017_03_21_031702_create_appointments_table', '4');
INSERT INTO `migrations` VALUES ('11', '2017_03_22_085521_create_repaireRecord_table', '5');
INSERT INTO `migrations` VALUES ('12', '2017_03_27_082546_create_delivers_table', '6');

-- ----------------------------
-- Table structure for mm_apply_records
-- ----------------------------
DROP TABLE IF EXISTS `mm_apply_records`;
CREATE TABLE `mm_apply_records` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `apply_type` tinyint(4) NOT NULL,
  `company_id` int(11) NOT NULL,
  `tree_trunk_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_type` smallint(6) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approvers` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statuses` tinyint(4) NOT NULL,
  `price` double NOT NULL,
  `quantity` mediumint(8) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` tinyint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_apply_records
-- ----------------------------
INSERT INTO `mm_apply_records` VALUES ('1', '1', '1', '1', '3', '办公桌', '2', '家具了', 'uuu', '0', '54', '1', 'test', '2017-03-13 16:54:23', '2017-04-18 17:11:21', '4');
INSERT INTO `mm_apply_records` VALUES ('2', '1', '1', '1', '3', '凳子', '2', '家具类', 'uuu', '0', '12', '3', 'test', '2017-03-13 16:58:48', '2017-04-18 17:11:33', '4');
INSERT INTO `mm_apply_records` VALUES ('3', '1', '1', '1', '3', 'Surface', '2', '平板电脑', 'uuu', '0', '12000', '1', 'test', '2017-03-14 11:00:08', '2017-03-14 11:00:08', '1');
INSERT INTO `mm_apply_records` VALUES ('4', '1', '1', '1', '3', 'Surface', '2', '平板电脑', 'uuu', '7', '12000', '1', 'test', '2017-03-14 11:04:40', '2017-04-16 10:42:39', '1');
INSERT INTO `mm_apply_records` VALUES ('5', '1', '1', '1', '3', 'Ipad AIr', '2', '平板电脑', 'uuu', '0', '2000', '1', 'test', '2017-03-14 11:07:29', '2017-03-14 11:07:29', '1');
INSERT INTO `mm_apply_records` VALUES ('6', '1', '1', '24', '3', 'Ipad AIr2', '2', '平板电脑', 'uuu', '0', '2000', '1', 'test', '2017-03-14 11:10:53', '2017-03-14 11:10:53', '1');
INSERT INTO `mm_apply_records` VALUES ('7', '1', '1', '24', '3', '游侠鼠标', '2', '鼠标', 'uuu', '0', '54', '1', 'test', '2017-03-15 09:47:00', '2017-03-15 09:47:00', '1');
INSERT INTO `mm_apply_records` VALUES ('8', '1', '1', '24', '3', '游侠鼠标2', '2', '鼠标', 'uuu', '0', '54', '1', 'test', '2017-03-15 09:47:42', '2017-03-15 09:47:42', '1');
INSERT INTO `mm_apply_records` VALUES ('9', '1', '1', '24', '3', '游侠鼠标3', '2', '鼠标', 'uuu', '0', '54', '1', 'test', '2017-03-15 09:50:05', '2017-03-15 09:50:05', '1');
INSERT INTO `mm_apply_records` VALUES ('10', '1', '1', '24', '3', '游侠鼠标4', '2', '鼠标', 'uuu', '0', '54', '1', 'test', '2017-03-15 09:50:46', '2017-03-15 09:50:46', '1');
INSERT INTO `mm_apply_records` VALUES ('11', '1', '1', '24', '3', '游侠鼠标5', '2', '鼠标', 'uuu', '0', '54', '1', 'test', '2017-03-15 09:55:22', '2017-03-15 09:55:22', '1');
INSERT INTO `mm_apply_records` VALUES ('12', '1', '1', '24', '3', '游侠鼠标6', '2', '鼠标', 'uuu', '0', '54', '1', 'test', '2017-03-15 09:59:34', '2017-03-15 09:59:34', '1');
INSERT INTO `mm_apply_records` VALUES ('13', '1', '1', '24', '3', '雷神游戏鼠标', '2', '游戏鼠标', 'uuu', '0', '84', '1', 'test', '2017-03-15 16:30:21', '2017-03-15 16:30:21', '1');
INSERT INTO `mm_apply_records` VALUES ('14', '1', '1', '24', '3', '现代操作系统', '2', '书籍', 'uuu', '0', '43', '1', 'test', '2017-03-15 16:32:32', '2017-03-15 16:32:32', '1');
INSERT INTO `mm_apply_records` VALUES ('15', '1', '1', '24', '3', '小米路由器', '2', '路由器', 'uuu', '0', '53', '1', 'test', '2017-03-15 16:44:36', '2017-03-15 16:44:36', '1');
INSERT INTO `mm_apply_records` VALUES ('16', '1', '1', '24', '3', '阿莫西林胶囊', '2', '感冒药', 'uuu', '0', '55', '1', 'test', '2017-03-15 17:11:52', '2017-03-15 17:11:52', '1');
INSERT INTO `mm_apply_records` VALUES ('17', '1', '1', '24', '3', '可可可乐', '2', '饮料类', 'uuu', '0', '3', '1', 'test', '2017-03-15 20:33:18', '2017-03-15 20:33:18', '1');
INSERT INTO `mm_apply_records` VALUES ('18', '1', '1', '24', '3', '可可可乐', '2', '饮料类', 'uuu', '0', '3', '1', 'test', '2017-03-15 20:36:14', '2017-03-15 20:36:14', '1');
INSERT INTO `mm_apply_records` VALUES ('19', '1', '1', '24', '3', '3D-Nand', '2', 'PC 存储', 'uuu', '0', '3000', '1', 'test', '2017-03-15 21:02:54', '2017-03-15 21:02:54', '1');
INSERT INTO `mm_apply_records` VALUES ('20', '1', '1', '24', '3', '4K显示器', '2', '显示器', 'uuu', '5', '5999', '1', 'test', '2017-03-15 21:04:18', '2017-04-16 10:42:32', '1');
INSERT INTO `mm_apply_records` VALUES ('21', '1', '1', '24', '3', 'A22轰炸机', '1', '武器类', 'uuu', '5', '50000000', '1', 'test', '2017-03-16 10:53:53', '2017-05-10 08:28:44', '1');
INSERT INTO `mm_apply_records` VALUES ('22', '1', '1', '24', '3', '数据库系统概论', '2', '书籍', 'uuu', '5', '65', '1', 'test', '2017-03-16 11:40:36', '2017-05-10 08:28:43', '1');
INSERT INTO `mm_apply_records` VALUES ('23', '1', '1', '24', '3', '挖掘机', '1', '重型机器', 'uuu', '0', '89000', '1', 'test', '2017-03-16 15:24:57', '2017-03-16 15:24:57', '1');
INSERT INTO `mm_apply_records` VALUES ('24', '1', '1', '0', '3', '吸尘器', '2', '清洁工具类', 'uuu', '0', '420', '1', '用于吸收地面灰尘', '2017-04-12 15:21:50', '2017-04-12 15:21:50', '1');
INSERT INTO `mm_apply_records` VALUES ('25', '1', '1', '0', '3', '魅蓝note4', '2', '智能手机', 'uuu', '0', '899', '1', '手机', '2017-04-12 16:21:19', '2017-04-12 16:21:19', '1');
INSERT INTO `mm_apply_records` VALUES ('26', '1', '1', '0', '3', '魅蓝2s', '2', '智能手机', 'uuu', '0', '599', '1', '魅族手机', '2017-04-12 16:36:25', '2017-04-12 16:36:25', '1');
INSERT INTO `mm_apply_records` VALUES ('27', '1', '1', '0', '3', '魅蓝3s', '2', '智能手机', 'uuu', '0', '599', '1', '没蓝手机', '2017-04-12 17:06:10', '2017-04-12 17:06:10', '1');
INSERT INTO `mm_apply_records` VALUES ('28', '1', '1', '0', '3', '魅蓝4s', '2', '智能手机', 'uuu', '0', '599', '1', '没蓝手机', '2017-04-12 17:09:18', '2017-04-12 17:09:18', '1');
INSERT INTO `mm_apply_records` VALUES ('29', '1', '1', '0', '3', '魅蓝5s', '2', '智能手机', 'uuu', '0', '599', '1', '没蓝手机', '2017-04-12 17:20:06', '2017-04-18 14:49:25', '4');
INSERT INTO `mm_apply_records` VALUES ('30', '1', '1', '0', '3', '魅蓝5s', '2', '智能手机', 'uuu', '7', '599', '1', '没蓝手机', '2017-04-12 17:20:49', '2017-04-18 17:11:04', '4');
INSERT INTO `mm_apply_records` VALUES ('31', '1', '1', '0', '3', '魅蓝6', '2', '智能手机', 'uuu', '7', '599', '1', '魅蓝手机', '2017-04-12 18:51:20', '2017-04-18 17:10:55', '4');
INSERT INTO `mm_apply_records` VALUES ('32', '1', '1', '0', '3', '魅蓝6s', '2', '智能手机', 'uuu', '7', '599', '1', '魅蓝手机', '2017-04-12 18:51:36', '2017-04-12 19:36:03', '1');
INSERT INTO `mm_apply_records` VALUES ('33', '1', '1', '0', '3', '魅蓝7', '2', '智能手机', 'uuu', '7', '599', '1', '魅蓝手机', '2017-04-12 18:54:15', '2017-04-12 19:36:00', '1');
INSERT INTO `mm_apply_records` VALUES ('34', '1', '1', '41', '38', 'R3008型时光机', '1', '时光机', 'uuu', '6', '900000', '1', 'test', '2017-04-13 17:12:42', '2017-08-28 00:54:17', '1');
INSERT INTO `mm_apply_records` VALUES ('35', '1', '1', '41', '38', 'T933-001割草机', '2', '割草机', 'uuu', '7', '1900', '1', '测试删除申请申请变化', '2017-04-13 19:47:30', '2017-04-17 09:36:12', '1');
INSERT INTO `mm_apply_records` VALUES ('36', '1', '1', '41', '38', 'T930-terminite-Robot', '2', '机器人', 'uuu', '7', '90333000', '1', '终结者', '2017-04-16 09:22:27', '2017-04-17 09:35:58', '1');
INSERT INTO `mm_apply_records` VALUES ('37', '1', '1', '41', '38', 'F22', '1', '轰炸机', 'uuu', '7', '90000', '1', '重型轰炸机', '2017-04-16 09:23:41', '2017-04-17 09:35:52', '1');
INSERT INTO `mm_apply_records` VALUES ('38', '1', '1', '41', '38', 'F35', '2', '战斗机', 'uuu', '6', '90000010', '1', '第四代轰炸机', '2017-04-16 09:37:05', '2017-08-31 22:58:48', '1');
INSERT INTO `mm_apply_records` VALUES ('39', '1', '1', '41', '38', 'F35', '2', '战斗机', 'uuu', '6', '90000010', '1', '第四代轰炸机', '2017-04-16 09:37:40', '2017-04-18 14:56:42', '1');
INSERT INTO `mm_apply_records` VALUES ('40', '1', '1', '41', '38', 'F30', '2', '战斗机', 'uuu', '4', '90000010', '1', '第四代轰炸机', '2017-04-16 09:39:02', '2017-04-18 14:52:25', '2');
INSERT INTO `mm_apply_records` VALUES ('41', '1', '1', '41', '38', '56式半自动步枪', '2', '武器类', 'uuu', '6', '8000', '1', '半自动步枪', '2017-04-16 20:32:44', '2018-01-08 06:48:02', '1');
INSERT INTO `mm_apply_records` VALUES ('42', '1', '1', '0', '38', '格力空调T8', '1', '空调', 'uuu', '6', '1900', '1', '空调', '2017-04-20 23:10:15', '2017-04-20 23:26:00', '1');
INSERT INTO `mm_apply_records` VALUES ('43', '1', '1', '0', '38', '格力中央空调C40', '1', '中央空调', 'uuu', '6', '9300', '1', '格力中央空调', '2017-04-21 12:18:44', '2017-09-22 07:29:49', '1');
INSERT INTO `mm_apply_records` VALUES ('44', '1', '1', '0', '38', '至真扫地机器人S-330', '1', '扫地机器人', 'uuu', '6', '2900', '1', '扫地机器人', '2017-04-21 16:52:04', '2017-05-10 03:52:50', '1');
INSERT INTO `mm_apply_records` VALUES ('45', '1', '1', '0', '38', 'IT项目管理', '2', '书', null, '6', '68', '1', 'IT项目管理类书籍', '2017-04-23 15:55:51', '2017-04-23 15:57:51', '1');
INSERT INTO `mm_apply_records` VALUES ('46', '1', '1', '0', '38', '巴雷特', '2', '狙', null, '3', '100000', '10', '狙击枪', '2017-05-20 01:03:09', '2017-05-20 01:22:29', '1');
INSERT INTO `mm_apply_records` VALUES ('47', '1', '1', '0', '38', '1111', '2', '2222', null, '6', '333', '444', '555', '2017-05-21 17:35:05', '2018-01-15 09:04:46', '1');
INSERT INTO `mm_apply_records` VALUES ('48', '1', '1', '0', '38', '测试一个', '2', '123', null, '4', '1', '1', '321313', '2017-06-05 18:25:25', '2017-06-25 17:39:55', '1');
INSERT INTO `mm_apply_records` VALUES ('49', '1', '1', '0', '38', 'iPhone8公务手机', '1', '电子产品', null, '6', '8000', '10', null, '2017-08-28 01:11:49', '2018-01-15 09:04:49', '1');
INSERT INTO `mm_apply_records` VALUES ('50', '1', '1', '0', '38', '333', '2', '333', null, '6', '333', '3', '333', '2017-08-31 22:58:26', '2018-01-07 06:42:28', '1');
INSERT INTO `mm_apply_records` VALUES ('51', '1', '1', '0', '38', 'SalvatoreFerragamo', '2', '333', null, '6', '0.011', '3', null, '2017-08-31 22:59:48', '2017-09-15 19:40:58', '1');
INSERT INTO `mm_apply_records` VALUES ('52', '1', '1', '0', '38', 'ThinkPad E470c 笔记本电脑', '1', '电脑', null, '4', '3499', '2', null, '2017-09-22 07:36:24', '2017-09-22 20:29:21', '1');

-- ----------------------------
-- Table structure for mm_appointments
-- ----------------------------
DROP TABLE IF EXISTS `mm_appointments`;
CREATE TABLE `mm_appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT '2017-03-20 11:44:29',
  `finish_time` timestamp NOT NULL DEFAULT '2017-03-20 11:44:29',
  `status` smallint(6) NOT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_appointments
-- ----------------------------
INSERT INTO `mm_appointments` VALUES ('1', '3', '1', '11', '2017-03-20 11:44:29', '2017-03-20 15:55:36', '2', '4');
INSERT INTO `mm_appointments` VALUES ('2', '3', '1', '11', '2017-03-20 11:44:29', '2017-03-20 15:59:06', '2', '4');
INSERT INTO `mm_appointments` VALUES ('3', '3', '1', '11', '2017-03-20 11:44:29', '2017-04-23 10:46:06', '2', '4');
INSERT INTO `mm_appointments` VALUES ('4', '3', '1', '11', '2017-03-20 11:44:29', '2017-03-21 14:21:14', '2', '4');
INSERT INTO `mm_appointments` VALUES ('5', '3', '1', '11', '2017-03-20 11:44:29', '2017-03-29 10:27:27', '2', '4');
INSERT INTO `mm_appointments` VALUES ('6', '3', '1', '7', '2017-03-20 11:44:29', '2017-03-27 17:01:27', '2', '1');
INSERT INTO `mm_appointments` VALUES ('7', '3', '1', '2', '2017-03-20 11:44:29', '2017-03-29 10:31:31', '2', '1');
INSERT INTO `mm_appointments` VALUES ('8', '3', '1', '2', '2017-03-20 11:44:29', '2017-03-29 10:43:40', '2', '1');
INSERT INTO `mm_appointments` VALUES ('9', '3', '1', '11', '2017-03-20 11:44:29', '2017-03-29 10:45:33', '2', '1');
INSERT INTO `mm_appointments` VALUES ('10', '3', '1', '11', '2017-03-20 11:44:29', '2017-03-29 10:47:29', '2', '1');
INSERT INTO `mm_appointments` VALUES ('11', '3', '1', '11', '2017-03-20 11:44:29', '2017-03-29 10:49:18', '2', '1');
INSERT INTO `mm_appointments` VALUES ('12', '3', '1', '2', '2017-03-20 11:44:29', '2017-03-29 11:31:09', '2', '1');
INSERT INTO `mm_appointments` VALUES ('13', '3', '1', '2', '2017-03-20 11:44:29', '2017-03-29 11:36:47', '2', '1');
INSERT INTO `mm_appointments` VALUES ('14', '3', '1', '3', '2017-03-20 11:44:29', '2017-03-29 11:36:35', '2', '1');
INSERT INTO `mm_appointments` VALUES ('15', '3', '1', '11', '2017-03-20 11:44:29', '2017-04-11 15:23:19', '2', '1');
INSERT INTO `mm_appointments` VALUES ('16', '3', '1', '7', '2017-03-20 11:44:29', '2017-04-11 15:23:20', '2', '1');
INSERT INTO `mm_appointments` VALUES ('17', '3', '1', '3', '2017-03-20 11:44:29', '2017-04-11 15:23:16', '2', '1');
INSERT INTO `mm_appointments` VALUES ('18', '3', '1', '2', '2017-03-20 11:44:29', '2017-04-11 15:23:22', '2', '1');
INSERT INTO `mm_appointments` VALUES ('19', '3', '1', '12', '2017-03-20 11:44:29', '2017-04-11 15:23:23', '2', '1');
INSERT INTO `mm_appointments` VALUES ('20', '3', '1', '13', '2017-03-20 11:44:29', '2017-04-11 15:23:24', '2', '1');
INSERT INTO `mm_appointments` VALUES ('21', '3', '1', '12', '2017-03-20 11:44:29', '2017-04-16 10:00:28', '2', '1');
INSERT INTO `mm_appointments` VALUES ('22', '3', '1', '7', '2017-03-20 11:44:29', '2017-04-12 10:32:53', '2', '1');
INSERT INTO `mm_appointments` VALUES ('23', '3', '1', '2', '2017-03-20 11:44:29', '2017-04-13 17:15:08', '2', '1');
INSERT INTO `mm_appointments` VALUES ('24', '3', '1', '3', '2017-03-20 11:44:29', '2017-04-13 19:18:27', '2', '1');
INSERT INTO `mm_appointments` VALUES ('25', '3', '1', '1', '2017-03-20 11:44:29', '2017-04-13 19:18:28', '2', '1');
INSERT INTO `mm_appointments` VALUES ('26', '3', '1', '13', '2017-03-20 11:44:29', '2017-04-21 15:32:37', '2', '1');
INSERT INTO `mm_appointments` VALUES ('27', '38', '1', '1', '2017-03-20 11:44:29', '2017-04-13 16:02:36', '2', '1');
INSERT INTO `mm_appointments` VALUES ('28', '38', '1', '1', '2017-03-20 11:44:29', '2017-04-13 17:08:04', '2', '1');
INSERT INTO `mm_appointments` VALUES ('29', '38', '1', '1', '2017-03-20 11:44:29', '2017-04-13 19:41:48', '2', '1');
INSERT INTO `mm_appointments` VALUES ('30', '38', '1', '1', '2017-03-20 11:44:29', '2017-04-17 09:19:58', '2', '1');
INSERT INTO `mm_appointments` VALUES ('31', '3', '1', '2', '2017-03-20 11:44:29', '2017-04-17 09:04:37', '2', '1');
INSERT INTO `mm_appointments` VALUES ('32', '38', '1', '3', '2017-03-20 11:44:29', '2017-04-21 16:16:31', '2', '1');
INSERT INTO `mm_appointments` VALUES ('33', '3', '1', '16', '2017-04-22 10:25:25', '2017-04-22 10:25:36', '2', '1');
INSERT INTO `mm_appointments` VALUES ('34', '3', '1', '3', '2017-04-23 10:12:20', '2017-04-23 10:41:13', '2', '1');
INSERT INTO `mm_appointments` VALUES ('35', '3', '1', '3', '2017-04-23 10:41:27', '2017-04-23 10:57:16', '2', '1');
INSERT INTO `mm_appointments` VALUES ('36', '3', '1', '3', '2017-04-23 10:58:16', '2017-04-23 13:24:09', '2', '1');
INSERT INTO `mm_appointments` VALUES ('37', '3', '1', '21', '2017-05-24 21:41:40', '2017-05-24 21:41:53', '2', '1');
INSERT INTO `mm_appointments` VALUES ('38', '3', '1', '21', '2017-05-24 17:40:40', '2017-08-08 18:47:41', '2', '1');

-- ----------------------------
-- Table structure for mm_company
-- ----------------------------
DROP TABLE IF EXISTS `mm_company`;
CREATE TABLE `mm_company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_company
-- ----------------------------
INSERT INTO `mm_company` VALUES ('1', 'aaa', 'testdata', '2017-03-03 19:16:33', '2017-03-03 19:16:42');
INSERT INTO `mm_company` VALUES ('2', '孙俪教育', null, '2017-04-22 18:51:08', '2017-04-22 18:51:08');

-- ----------------------------
-- Table structure for mm_config
-- ----------------------------
DROP TABLE IF EXISTS `mm_config`;
CREATE TABLE `mm_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_tree_trunk_id` int(11) DEFAULT NULL,
  `config_company_id` int(11) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_config
-- ----------------------------
INSERT INTO `mm_config` VALUES ('1', 'tree_trunk#type', '1', 'organization', '1', null, 'name 中 # 前一部分表示表名，后一部分表示字段名，tree_trunk的 type 字段 1 表示是组织机构', null, '2017-03-09 04:43:02', null);
INSERT INTO `mm_config` VALUES ('2', 'tree_trunk#type', '2', 'materialType1', '1', null, 'type的值为2表示为设备的类别', null, '2017-03-09 04:44:53', null);
INSERT INTO `mm_config` VALUES ('3', 'default_rent_time', 'day', '10', null, '1', '为租借设备配置默认的租借时间', null, null, null);
INSERT INTO `mm_config` VALUES ('4', 'material_repaired', '武器类', '3', null, null, '武器类的设备设置有 id 号为3 的 用户维修', null, null, null);
INSERT INTO `mm_config` VALUES ('5', 'material_repaired', 'all', 'manager', null, null, '默认设备维修由当前层的管理人员来维修', null, null, null);

-- ----------------------------
-- Table structure for mm_delivers
-- ----------------------------
DROP TABLE IF EXISTS `mm_delivers`;
CREATE TABLE `mm_delivers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `using_record_id` int(11) NOT NULL,
  `accepter_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `phone` char(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `user_comment` text COLLATE utf8mb4_unicode_ci,
  `deliver_comment` text COLLATE utf8mb4_unicode_ci,
  `arrival_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deliver_man_id` int(11) DEFAULT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_delivers
-- ----------------------------
INSERT INTO `mm_delivers` VALUES ('1', '24', 'uuu', '15278338882', '湖北省-武汉市-江岸区-黄冈镇7号大街', '3', null, null, '2017-04-19 01:16:54', '3', '4');
INSERT INTO `mm_delivers` VALUES ('2', '25', '', '17378118873', '湖南省-长沙市-芙蓉区-潇湘大街-帝国大厦-7楼-12号', '3', null, null, '2017-04-19 01:16:55', '3', '4');
INSERT INTO `mm_delivers` VALUES ('3', '26', '', '17378117082', '山东省-济南市-历下区-新乡噗噗街-angle 大楼-12层-21号', '3', null, null, '2017-03-29 18:33:37', '3', '1');
INSERT INTO `mm_delivers` VALUES ('4', '27', '', '18191278373', '江西省-南昌市-东湖区-琥珀解-晓辉大楼-23成-8号门', '3', null, null, '2017-03-29 18:54:54', '3', '1');
INSERT INTO `mm_delivers` VALUES ('5', '28', '', '1783782273', '河北省-石家庄市-长安区-天津大学', '3', null, null, '2017-03-29 22:46:58', '3', '1');
INSERT INTO `mm_delivers` VALUES ('6', '35', '', '18737882738', '山西省-太原市-小店区-西安电子科技大学', '2', null, null, '2017-03-29 22:44:47', '3', '1');
INSERT INTO `mm_delivers` VALUES ('7', '36', '', '17878227793', '浙江省-杭州市-上城区-浙江大学', '3', null, null, '2017-03-29 23:06:41', '3', '1');
INSERT INTO `mm_delivers` VALUES ('8', '37', '', '17378118015', '北京市-北京市市辖区-东城区-思茅从街', '3', null, null, '2017-04-11 17:19:25', '3', '1');
INSERT INTO `mm_delivers` VALUES ('9', '38', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 17:19:30', '3', '1');
INSERT INTO `mm_delivers` VALUES ('10', '39', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 17:25:46', '3', '1');
INSERT INTO `mm_delivers` VALUES ('11', '40', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 17:25:48', '3', '1');
INSERT INTO `mm_delivers` VALUES ('12', '41', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:03:47', '3', '1');
INSERT INTO `mm_delivers` VALUES ('13', '42', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:03:49', '3', '1');
INSERT INTO `mm_delivers` VALUES ('14', '43', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:05:36', '3', '1');
INSERT INTO `mm_delivers` VALUES ('15', '44', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:11:53', '3', '1');
INSERT INTO `mm_delivers` VALUES ('16', '45', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:27:58', '3', '1');
INSERT INTO `mm_delivers` VALUES ('17', '46', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:27:59', '3', '1');
INSERT INTO `mm_delivers` VALUES ('18', '47', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:28:00', '3', '1');
INSERT INTO `mm_delivers` VALUES ('19', '48', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:34:34', '3', '1');
INSERT INTO `mm_delivers` VALUES ('20', '49', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:34:35', '3', '1');
INSERT INTO `mm_delivers` VALUES ('21', '50', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:34:36', '3', '1');
INSERT INTO `mm_delivers` VALUES ('22', '51', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:34:38', '3', '1');
INSERT INTO `mm_delivers` VALUES ('23', '52', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 19:34:41', '3', '1');
INSERT INTO `mm_delivers` VALUES ('24', '53', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 22:54:16', '3', '1');
INSERT INTO `mm_delivers` VALUES ('25', '54', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 22:49:51', '3', '1');
INSERT INTO `mm_delivers` VALUES ('26', '55', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 22:49:52', '3', '1');
INSERT INTO `mm_delivers` VALUES ('27', '56', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 22:49:53', '3', '1');
INSERT INTO `mm_delivers` VALUES ('28', '57', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 22:49:54', '3', '1');
INSERT INTO `mm_delivers` VALUES ('29', '58', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-11 23:25:11', '3', '1');
INSERT INTO `mm_delivers` VALUES ('30', '69', '', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-14 03:17:35', '3', '1');
INSERT INTO `mm_delivers` VALUES ('31', '71', 'uuu', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '2', null, null, '2017-04-17 06:06:18', '3', '1');
INSERT INTO `mm_delivers` VALUES ('32', '74', '阿狸', '17378118015', '陕西省-西安市-长安区-西安电子科技大学', '3', null, null, '2017-04-20 18:19:12', '3', '1');
INSERT INTO `mm_delivers` VALUES ('34', '76', '阿狸', '17378118015', '陕西省-西安市-长安区-西安电子科技大学', '3', null, null, '2017-04-19 19:44:27', '3', '1');
INSERT INTO `mm_delivers` VALUES ('35', '77', 'uuu', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-20 18:37:03', '3', '1');
INSERT INTO `mm_delivers` VALUES ('36', '78', 'uuu', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-20 18:40:37', '3', '1');
INSERT INTO `mm_delivers` VALUES ('37', '79', 'nash', '18778118015', '山东省-济南市-历下区-西安电子科技大学', '3', null, null, '2017-04-20 18:45:27', '3', '1');
INSERT INTO `mm_delivers` VALUES ('38', '80', 'uuu', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-21 05:59:47', '3', '1');
INSERT INTO `mm_delivers` VALUES ('39', '81', 'uuu', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-22 05:15:44', '3', '1');
INSERT INTO `mm_delivers` VALUES ('40', '82', '阿狸', '17378118015', '陕西省-西安市-长安区-西安电子科技大学', '3', null, null, '2017-04-22 04:59:55', '3', '1');
INSERT INTO `mm_delivers` VALUES ('41', '83', '阿狸', '17378118015', '陕西省-西安市-长安区-西安电子科技大学', '3', null, null, '2017-04-22 04:47:05', '3', '1');
INSERT INTO `mm_delivers` VALUES ('42', '84', 'uuu', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '2', null, null, '2017-05-10 07:26:43', '3', '1');
INSERT INTO `mm_delivers` VALUES ('43', '85', '阿狸', '17378118015', '陕西省-西安市-长安区-西安电子科技大学', '3', null, null, '2017-04-22 05:41:21', '3', '1');
INSERT INTO `mm_delivers` VALUES ('44', '86', '阿狸', '17378118015', '陕西省-西安市-长安区-西安电子科技大学', '3', null, null, '2017-04-23 21:12:28', '3', '1');
INSERT INTO `mm_delivers` VALUES ('45', '87', 'uuu', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-23 18:02:13', '3', '1');
INSERT INTO `mm_delivers` VALUES ('46', '88', 'uuu', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-04-23 18:59:44', '3', '1');
INSERT INTO `mm_delivers` VALUES ('47', '91', '孙俪', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '1', null, null, null, null, '1');
INSERT INTO `mm_delivers` VALUES ('48', '94', '孙俪', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '1', null, null, null, null, '1');
INSERT INTO `mm_delivers` VALUES ('49', '95', '赵信', '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '3', null, null, '2017-06-04 15:00:46', '3', '1');

-- ----------------------------
-- Table structure for mm_material
-- ----------------------------
DROP TABLE IF EXISTS `mm_material`;
CREATE TABLE `mm_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '只保存图片的相对路径，相对于用户指定的文件夹目录',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `material_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tree_trunk_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `sort` tinyint(3) unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture_url` text COLLATE utf8mb4_unicode_ci COMMENT '图片对应的 url , 只保存相对路径。',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT '1' COMMENT '参考ModelUtil 的 delete 常量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_material
-- ----------------------------
INSERT INTO `mm_material` VALUES ('1', 'table', 'table2', '41', '1', '1', 'huiuddw', '1', null, '家具', '122', '1', '2017-03-08 09:11:09', '2017-04-20 21:44:21', '4');
INSERT INTO `mm_material` VALUES ('2', 'table', '1hui', '0', '1', '19', 'huiuddw', '1', 'bbcd531ecbee8bc22e1204e1debc78bf_png', '家具', '122', '2', '2017-03-08 09:12:13', '2017-04-20 21:45:51', '4');
INSERT INTO `mm_material` VALUES ('3', 'Zenter', 'Server-IBM-0001', '0', '1', '5', 'hiujd', '1', 'de9f758b08fe8ea9ccbb406ccb39f93b_jpg', 'IBM大型服务器', '1134000', '1', '2017-03-08 09:13:05', '2017-09-22 07:30:30', '1');
INSERT INTO `mm_material` VALUES ('7', 'testjjj', 'u33', '0', '1', '18', 'uuii', '1', '1bc32b5cbf9e2ee73e678757ef21f42c_png', 'hu333', '333', '4', '2017-03-08 09:34:49', '2017-04-23 17:52:41', '4');
INSERT INTO `mm_material` VALUES ('8', 'yyyyyyyyy', '123', '25', '1', '7', 'huuu', '1', null, 'y', '333', '1', '2017-03-08 09:35:46', '2017-05-30 01:41:32', '4');
INSERT INTO `mm_material` VALUES ('9', '坦克', '1123', '25', '1', '8', '12333', '1', null, '武器类', '12333', '1', '2017-03-08 09:47:49', '2017-05-30 01:41:32', '4');
INSERT INTO `mm_material` VALUES ('10', 'Uta卡车', 'kache-0001', '0', '1', '6', 'UUU', '1', '93fc9b044ce5fff8f95e71fdd558df0e_jpg', '三一重工卡车', '123000', '3', '2017-03-13 10:45:22', '2018-01-10 08:28:47', '1');
INSERT INTO `mm_material` VALUES ('11', '火箭筒', 'daodan123', '0', '1', '20', 'test', '2', '33c0ce2750b0b0217a7b537539c6384c_jpg', '武器类', '9000', '4', '2017-03-16 16:40:22', '2017-04-22 11:35:49', '4');
INSERT INTO `mm_material` VALUES ('12', '95式坦克', '123', '44', '1', '0', '1234', '2', null, '武器类', '123', '4', '2017-03-18 16:31:51', '2017-04-20 10:11:24', '4');
INSERT INTO `mm_material` VALUES ('13', '系统测试要领', 'TP312.1930', '44', '1', '0', '软件测试类书籍', '2', 'b7774b726e3e723330b99b75c0bcee1d_png', '书籍', '38', '1', '2017-04-10 10:01:37', '2017-04-20 10:11:24', '4');
INSERT INTO `mm_material` VALUES ('14', '宇宙飞船', 'f000001', '47', '1', '1', '测试', '2', 'e336356bae91a778bd354b267fa06493_PNG', '飞船', '90000000', '1', '2017-04-18 21:58:30', '2017-04-18 21:58:55', '4');
INSERT INTO `mm_material` VALUES ('15', 'ABB-T300 机械臂', 'ARM5000', '42', '1', '0', '工业操作的机械臂', '1', '8d3a7198cb02d50979e7e36318b3c0e7_png', '机械臂', '3900', '2', '2017-04-20 10:44:05', '2017-05-30 18:06:24', '1');
INSERT INTO `mm_material` VALUES ('16', 'T900终结者', 'T90002', '41', '1', '1', '终结者机器人', '2', 'd413d52590d16280bbfa57c65cfef1c1_jpg', '机器人', '90000000', '1', '2017-04-21 16:21:21', '2018-01-05 16:33:57', '1');
INSERT INTO `mm_material` VALUES ('17', 'T900终结者2', 'T90003', '41', '1', '2', '终结者机器人', '2', '6da04fb01d95ac2ec0a5ca3b5c1bc9c6_jpg', '终结者', '9000000', '3', '2017-04-21 16:24:22', '2017-05-30 18:12:39', '1');
INSERT INTO `mm_material` VALUES ('18', '爱国者导弹D-8008', 'WD-8933', '46', '1', '0', '精确制导导弹', '1', 'fe1bb5d594920b8297fd068386ebfe40_PNG', '武器类', '900000', '2', '2017-04-21 17:50:14', '2017-05-30 01:41:59', '4');
INSERT INTO `mm_material` VALUES ('19', 'T900液体终结者', 'TY-9000', '42', '1', '0', '液体终结者', '1', '2e471402d44488a961102afbe08f8ada_jpg', '终结者', '9000000', '1', '2017-04-21 21:39:35', '2017-05-30 18:07:45', '1');
INSERT INTO `mm_material` VALUES ('20', '投影仪T-8900', 'TYY-1', '86', '2', '0', '多媒体投影仪', '1', '932cb35c888192beafa7c6c40d98a761_PNG', '投影仪', '4900', '2', '2017-04-22 21:20:27', '2017-05-24 23:21:10', '1');
INSERT INTO `mm_material` VALUES ('21', '高等数学', 'B-1320', '0', '1', '7', '本科高等数学', '2', 'a578ca3b74b05408c0d7eb4a8a3fad26_jpg', '教科书', '40', '1', '2017-04-27 21:12:11', '2018-01-03 19:08:37', '1');
INSERT INTO `mm_material` VALUES ('22', '戴尔1U服务器', '001', '91', '1', '0', '戴尔R730', '2', null, '服务器', '30000', '1', '2018-01-05 16:39:19', '2018-01-05 16:41:29', '1');

-- ----------------------------
-- Table structure for mm_messages
-- ----------------------------
DROP TABLE IF EXISTS `mm_messages`;
CREATE TABLE `mm_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` smallint(6) NOT NULL COMMENT '表示消息的类型， 1 表示通知消息，2 表示 留言消息，',
  `sender_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL COMMENT '1 表示未读消息，2 表示已读消息',
  `addition` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_messages
-- ----------------------------
INSERT INTO `mm_messages` VALUES ('1', '1', '3', '38', '您好 ！ <br> 已经 分派 <br>在购买iPhone8公务手机的申请', '3', '1', '', '2017-12-30 14:12:47', '2018-01-03 19:09:37');
INSERT INTO `mm_messages` VALUES ('2', '1', '3', '3', '<div class=\"ibox-content\">\n					<h4>物资故障信息</h4>\n                        <table class=\"table\">\n					 		<thead>\n                                <tr>\n                                    <th>部门</th>\n                                    <th>申报人</th>\n                                    <th>物资名称</th>\n                                    <th>物资编号</th>\n                                    <th>资产类型</th>\n                                    <th>物资类别</th>\n                                    <th>故障描述</th>\n                                    <th>上报时间</th>\n                                </tr>\n                            </thead>\n                            <tbody>\n                                <tr>\n                                    <td>aaa</td>\n                                    <td>小游</td>\n                                    <td>Uta卡车</td>\n                                    <td></td>\n                                    <td>1</td>\n                                    <td>三一重工卡车</td>\n                                    <td>aaa</td>\n                                    <td>2018-01-10 08:28:47</td>\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                    ', '3', '1', '', '2018-01-10 08:28:47', '2018-01-11 07:08:21');
INSERT INTO `mm_messages` VALUES ('3', '1', '3', '38', '您好 ！ <br> 已经 分派 <br>在购买1111的申请', '3', '0', '', '2018-01-15 09:01:52', '2018-01-15 09:01:52');

-- ----------------------------
-- Table structure for mm_repaire_record
-- ----------------------------
DROP TABLE IF EXISTS `mm_repaire_record`;
CREATE TABLE `mm_repaire_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `material_id` int(11) NOT NULL,
  `fault_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_repaire_record
-- ----------------------------
INSERT INTO `mm_repaire_record` VALUES ('1', '3', '1', '7', 'testjjjj 发生了故障', '3', '2017-03-21 21:09:16', '2017-03-22 10:52:09', '1');
INSERT INTO `mm_repaire_record` VALUES ('2', '3', '1', '7', 'testjjj 故障了', '3', '2017-03-21 21:11:36', '2017-03-29 09:58:48', '1');
INSERT INTO `mm_repaire_record` VALUES ('3', '3', '1', '7', 'testjjj', '3', '2017-03-21 21:17:52', '2017-03-29 10:20:57', '1');
INSERT INTO `mm_repaire_record` VALUES ('4', '3', '1', '7', 'testjjj 故障了', '3', '2017-03-21 21:21:08', '2017-03-29 10:26:56', '1');
INSERT INTO `mm_repaire_record` VALUES ('5', '3', '1', '11', '燃料泄露', '3', '2017-03-29 10:31:22', '2017-04-18 16:51:47', '4');
INSERT INTO `mm_repaire_record` VALUES ('6', '3', '1', '7', '报修测试', '3', '2017-04-10 16:39:01', '2017-04-10 16:39:31', '1');
INSERT INTO `mm_repaire_record` VALUES ('7', '3', '1', '7', '报修测试2', '3', '2017-04-10 16:39:56', '2017-04-10 16:47:40', '1');
INSERT INTO `mm_repaire_record` VALUES ('8', '3', '1', '7', '使用的过程中突然坏了', '3', '2017-04-13 09:22:00', '2017-04-13 10:04:04', '1');
INSERT INTO `mm_repaire_record` VALUES ('9', '3', '1', '7', '发生了故障', '3', '2017-04-13 10:18:00', '2017-04-13 11:41:26', '1');
INSERT INTO `mm_repaire_record` VALUES ('10', '3', '1', '3', '发生了大爆炸', '3', '2017-04-16 22:11:58', '2017-04-16 22:27:30', '1');
INSERT INTO `mm_repaire_record` VALUES ('11', '3', '1', '7', '发生了大爆炸', '3', '2017-04-16 22:12:57', '2017-04-16 22:29:20', '1');
INSERT INTO `mm_repaire_record` VALUES ('12', '3', '1', '7', '发生了大爆炸', '3', '2017-04-16 22:14:21', '2017-04-23 17:22:50', '1');
INSERT INTO `mm_repaire_record` VALUES ('13', '3', '1', '7', '发生了大爆炸', '3', '2017-04-16 22:16:33', '2017-04-23 17:18:44', '1');
INSERT INTO `mm_repaire_record` VALUES ('14', '3', '1', '7', '测试报废', '3', '2017-04-16 22:34:54', '2017-04-16 22:35:06', '1');
INSERT INTO `mm_repaire_record` VALUES ('15', '3', '1', '7', '不能正常运行', '3', '2017-04-17 09:15:50', '2017-04-17 09:24:47', '1');
INSERT INTO `mm_repaire_record` VALUES ('16', '38', '1', '1', '固定桌角的螺丝缺失一个', '3', '2017-04-17 09:19:40', '2017-04-17 10:32:30', '1');
INSERT INTO `mm_repaire_record` VALUES ('17', '3', '1', '1', '部件缺失', '3', '2017-04-17 09:25:59', '2017-04-17 09:26:10', '1');
INSERT INTO `mm_repaire_record` VALUES ('18', '38', '1', '1', '部件缺失', '3', '2017-04-17 09:27:12', '2017-04-17 10:32:31', '1');
INSERT INTO `mm_repaire_record` VALUES ('19', '38', '1', '7', '冒烟了', '3', '2017-04-17 10:33:39', '2017-04-22 22:22:39', '1');
INSERT INTO `mm_repaire_record` VALUES ('20', '3', '1', '7', '发生了故障', '3', '2017-04-23 11:02:08', '2017-04-23 11:33:05', '1');
INSERT INTO `mm_repaire_record` VALUES ('21', '3', '1', '7', '又发生故障了', '3', '2017-04-23 11:33:24', '2017-04-23 12:52:10', '1');
INSERT INTO `mm_repaire_record` VALUES ('22', '3', '1', '7', '第三次故障', '3', '2017-04-23 12:54:42', '2017-04-23 12:54:53', '1');
INSERT INTO `mm_repaire_record` VALUES ('23', '3', '1', '7', '第三次发生故障了', '5', '2017-04-23 13:00:04', '2017-04-23 17:07:09', '4');
INSERT INTO `mm_repaire_record` VALUES ('24', '3', '1', '3', '测试发生故障了', '3', '2017-04-23 17:06:35', '2017-04-23 17:06:46', '1');
INSERT INTO `mm_repaire_record` VALUES ('25', '3', '1', '7', '测试发生报废', '5', '2017-04-23 17:19:38', '2017-04-23 17:19:42', '4');
INSERT INTO `mm_repaire_record` VALUES ('26', '3', '1', '7', '测试报废2', '3', '2017-04-23 17:23:02', '2017-04-23 17:51:37', '1');
INSERT INTO `mm_repaire_record` VALUES ('27', '3', '1', '7', '测试报废即删除', '5', '2017-04-23 17:52:35', '2017-04-23 17:52:41', '1');
INSERT INTO `mm_repaire_record` VALUES ('28', '3', '1', '3', '发送了轻微故障', '3', '2017-05-06 04:58:01', '2017-05-10 03:43:18', '1');
INSERT INTO `mm_repaire_record` VALUES ('29', '37', '1', '16', '保修，该理发了', '3', '2017-05-20 00:27:22', '2017-05-20 00:27:47', '1');
INSERT INTO `mm_repaire_record` VALUES ('30', '3', '1', '17', '111', '1', '2017-05-25 05:46:18', '2017-07-02 21:04:05', '4');
INSERT INTO `mm_repaire_record` VALUES ('31', '3', '1', '21', '刚刚好', '3', '2017-08-31 07:18:01', '2017-08-31 07:18:05', '1');
INSERT INTO `mm_repaire_record` VALUES ('32', '3', '1', '10', '得到', '3', '2017-08-31 07:18:58', '2017-08-31 07:19:41', '1');
INSERT INTO `mm_repaire_record` VALUES ('33', '3', '1', '10', 'aaa', '1', '2018-01-10 08:28:47', '2018-01-10 08:28:47', '1');

-- ----------------------------
-- Table structure for mm_tree_trunk
-- ----------------------------
DROP TABLE IF EXISTS `mm_tree_trunk`;
CREATE TABLE `mm_tree_trunk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL COMMENT '具体表示由config表决定，例如：1 表示组织机构，2 表示仓库的分类，3 表示物资的分类',
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT '1' COMMENT '在同一级的树形结构中的排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `contacts` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '如果 当前记录表示公司的部门，则contacts表示部门的联系人。否则此字段为空',
  `contacts_phone` char(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT '1' COMMENT '参考 ModelUtil 的 delete 字段常量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_tree_trunk
-- ----------------------------
INSERT INTO `mm_tree_trunk` VALUES ('24', '主管部门', '1', '11', '0', '1', '1', '2017-03-07 20:59:21', '2017-04-23 14:31:26', '', null, '1111', '4');
INSERT INTO `mm_tree_trunk` VALUES ('25', 'abc', '1', '12', '0', '1', '0', '2017-03-07 23:27:54', '2017-05-30 01:41:32', '', null, '1223', '4');
INSERT INTO `mm_tree_trunk` VALUES ('26', 'bcd', '1', '123', '25', '1', '0', '2017-03-07 23:29:12', '2017-05-30 01:41:32', '', null, '1233', '4');
INSERT INTO `mm_tree_trunk` VALUES ('27', 'hhhhhd', '1', 'hhh', '26', '1', '1', '2017-03-07 23:35:09', '2017-05-30 01:41:32', '', null, 'hhh', '4');
INSERT INTO `mm_tree_trunk` VALUES ('28', 'xxxx', '1', 'hh112', '26', '1', '1', '2017-03-07 23:36:04', '2017-05-30 01:41:32', '', null, '222', '4');
INSERT INTO `mm_tree_trunk` VALUES ('29', 'abchhhhd', '1', 'hhh', '25', '1', '1', '2017-03-07 23:45:30', '2017-05-30 01:41:32', '', null, '1hh', '4');
INSERT INTO `mm_tree_trunk` VALUES ('30', 'xxxx', '1', 'hhh1', '25', '1', '2', '2017-03-07 23:50:01', '2017-05-30 01:41:32', '', null, 'hhhh', '4');
INSERT INTO `mm_tree_trunk` VALUES ('31', 'xx22', '1', 'hh', '25', '1', '3', '2017-03-07 23:51:22', '2017-05-30 01:41:32', '', null, '2hh', '4');
INSERT INTO `mm_tree_trunk` VALUES ('32', 'xx333', '1', '1111', '25', '1', '4', '2017-03-07 23:55:21', '2017-05-30 01:41:32', '', null, '11112', '4');
INSERT INTO `mm_tree_trunk` VALUES ('33', 'xxx444', '1', '11', '31', '1', '1', '2017-03-08 00:01:16', '2017-05-30 01:41:32', '', null, '1111', '4');
INSERT INTO `mm_tree_trunk` VALUES ('34', 'xxx555', '1', '1hhh', '31', '1', '1', '2017-03-08 00:07:21', '2017-05-30 01:41:32', '', null, '1hhhh', '4');
INSERT INTO `mm_tree_trunk` VALUES ('35', 'xx8888', '1', 'hhh', '33', '1', '1', '2017-03-08 00:08:46', '2017-05-30 01:41:32', '', null, 'huidd', '4');
INSERT INTO `mm_tree_trunk` VALUES ('37', 'hjiuye', '1', '111h', '25', '1', '5', '2017-03-08 08:59:14', '2017-05-30 01:41:32', '', null, 'hjj', '4');
INSERT INTO `mm_tree_trunk` VALUES ('41', 'huiroot-parent', '1', 'hui', '0', '1', '4', '2017-03-08 09:08:37', '2017-05-30 18:12:39', '', null, 'huiuuu', '1');
INSERT INTO `mm_tree_trunk` VALUES ('42', 'huiu', '1', 'uiu', '41', '1', '0', '2017-03-08 09:37:13', '2017-05-30 18:12:39', '', null, 'hiuuu', '1');
INSERT INTO `mm_tree_trunk` VALUES ('44', '测试部', '1', 'che1', '0', '1', '14', '2017-03-08 09:40:07', '2017-04-20 10:11:24', '', null, '1123', '4');
INSERT INTO `mm_tree_trunk` VALUES ('45', 'java测试部', '1', 'jche1', '44', '1', '1', '2017-03-08 09:42:10', '2017-04-20 10:11:24', '', null, '122', '4');
INSERT INTO `mm_tree_trunk` VALUES ('46', '导弹部', '1', 'dao1', '0', '1', '3', '2017-03-08 09:43:25', '2017-05-30 01:41:59', '', null, 'huu', '4');
INSERT INTO `mm_tree_trunk` VALUES ('47', '行天部', '1', 'H311', '25', '1', '6', '2017-03-08 09:44:20', '2017-04-18 21:58:55', '', null, 'TEST', '4');
INSERT INTO `mm_tree_trunk` VALUES ('48', 'test---', '1', 'hui', '0', '1', '4', '2017-03-08 11:12:26', '2017-05-30 01:42:14', '', null, 'huui', '4');
INSERT INTO `mm_tree_trunk` VALUES ('49', 'uuuu878', '1', 'hu12', '0', '1', '0', '2017-03-08 11:14:50', '2017-05-30 18:12:39', '', null, '12', '1');
INSERT INTO `mm_tree_trunk` VALUES ('50', 'hhhhhhh22222222222', '1', 'uu', '47', '1', '0', '2017-03-08 11:15:51', '2017-04-18 21:58:55', '', null, 'uuu', '4');
INSERT INTO `mm_tree_trunk` VALUES ('51', 'testchildren', '1', 'hhu', '0', '1', '1', '2017-03-08 11:18:46', '2017-05-30 01:41:47', '', null, 'uuu', '4');
INSERT INTO `mm_tree_trunk` VALUES ('52', 'testchiled21', '1', '11', '51', '1', '1', '2017-03-08 11:20:18', '2017-05-30 01:41:47', '', null, '111', '4');
INSERT INTO `mm_tree_trunk` VALUES ('53', 'testmuchichildren', '1', '11', '0', '1', '1', '2017-03-08 11:20:56', '2017-05-30 18:12:39', '', null, 'huuu', '1');
INSERT INTO `mm_tree_trunk` VALUES ('54', 'test-----is_open', '1', 'huu', '0', '1', '3', '2017-03-08 11:26:28', '2017-05-30 18:12:39', '', null, 'huuu', '1');
INSERT INTO `mm_tree_trunk` VALUES ('55', 'test---is_open-----opened', '2', '1hhu', '0', '1', '15', '2017-03-08 11:27:50', '2017-04-27 21:10:55', '', null, 'huu', '4');
INSERT INTO `mm_tree_trunk` VALUES ('56', 'test---open_node', '1', '111', '0', '1', '13', '2017-03-08 11:32:45', '2017-04-20 10:12:21', '', null, 'huuu', '4');
INSERT INTO `mm_tree_trunk` VALUES ('62', 'hhhhhhh', '1', 'hhhh', '81', '1', '2', '2017-03-08 15:44:36', '2017-04-23 14:31:46', '', null, 'hhhh', '4');
INSERT INTO `mm_tree_trunk` VALUES ('63', 'iiiiii', '1', 'iiii', '81', '1', '3', '2017-03-08 15:45:29', '2017-04-23 14:31:46', '', null, 'iiii', '4');
INSERT INTO `mm_tree_trunk` VALUES ('64', 'lllllll', '1', 'lllll', '0', '1', '8', '2017-03-08 15:48:21', '2017-04-23 14:32:01', '', null, 'llll', '4');
INSERT INTO `mm_tree_trunk` VALUES ('65', 'jjjjjjjjjjj', '1', 'jjjjjjjjjjj', '81', '1', '4', '2017-03-08 15:49:12', '2017-04-23 14:31:46', '', null, 'jjjjjjjjjjjjjj', '4');
INSERT INTO `mm_tree_trunk` VALUES ('66', 'kkkkkkkkkkkkkkkkk', '1', 'kkkkkkk', '65', '1', '1', '2017-03-08 15:49:46', '2017-04-23 14:31:46', '', null, 'kkkkkkkkkkkk', '4');
INSERT INTO `mm_tree_trunk` VALUES ('67', 'mmmmmmmmmmmmmmm', '1', 'mmmmmmm', '65', '1', '1', '2017-03-08 15:50:36', '2017-04-23 14:31:46', '', null, 'mmmmmmm', '4');
INSERT INTO `mm_tree_trunk` VALUES ('70', 'ppppppppppppp', '1', 'ppppppppppppppp', '0', '1', '7', '2017-03-08 16:00:16', '2017-04-23 14:31:55', '', null, 'pppppppppppp', '4');
INSERT INTO `mm_tree_trunk` VALUES ('73', 'xxxxxxxxxxxx', '1', 'xxx', '81', '1', '1', '2017-03-08 22:56:15', '2017-04-21 15:28:36', '', null, 'xxxxxxxx', '4');
INSERT INTO `mm_tree_trunk` VALUES ('74', 'ccc', '1', 'xxxxxxxxx', '0', '1', '2', '2017-03-09 09:24:32', '2017-05-30 01:41:53', '', null, 'xxxxxxxxxxxxxxxx', '4');
INSERT INTO `mm_tree_trunk` VALUES ('75', '1234', '1', '1234', '81', '1', '0', '2017-03-13 10:24:44', '2017-04-23 14:31:46', '', null, '1234', '4');
INSERT INTO `mm_tree_trunk` VALUES ('76', '1123', '2', '123222', '62', '1', '0', '2017-03-13 10:39:34', '2017-04-23 14:31:46', '', null, '1234', '4');
INSERT INTO `mm_tree_trunk` VALUES ('77', '1234456', '1', '123456', '62', '1', '1', '2017-03-13 10:40:08', '2017-04-23 14:31:46', '', null, '1234', '4');
INSERT INTO `mm_tree_trunk` VALUES ('78', 'uuu', '2', '1233uu', '77', '1', '1', '2017-03-13 10:40:44', '2017-04-23 14:31:46', '', null, 'uuu', '4');
INSERT INTO `mm_tree_trunk` VALUES ('79', '黑色系', '2', 'hei12', '0', '1', '3', '2017-03-16 16:42:59', '2017-05-30 01:42:06', '', null, 'test', '4');
INSERT INTO `mm_tree_trunk` VALUES ('80', '测试select', '2', 'test', '0', '1', '2', '2017-03-18 16:54:18', '2017-05-30 18:12:39', '', null, 'test', '1');
INSERT INTO `mm_tree_trunk` VALUES ('81', '测试分支部门', '1', 'che123', '0', '1', '4', '2017-03-19 15:07:35', '2017-04-23 14:31:46', '', null, 'test', '4');
INSERT INTO `mm_tree_trunk` VALUES ('82', '程序测试', '2', 'CH284.1883', '48', '1', '1', '2017-04-12 14:51:46', '2017-05-30 01:42:14', '', null, 'test', '4');
INSERT INTO `mm_tree_trunk` VALUES ('83', '测试统一删除的修改', '2', 'Test-modify-1', '25', '1', '1', '2017-04-18 21:05:21', '2017-04-18 21:48:38', '', null, 'test2', '4');
INSERT INTO `mm_tree_trunk` VALUES ('84', '大型器械类', '2', 'Big-1', '81', '1', '1', '2017-04-21 15:10:42', '2017-04-23 14:31:46', '', null, '大型器械类的分支', '4');
INSERT INTO `mm_tree_trunk` VALUES ('85', '人事部', '1', 'HM-1', '0', '1', '2', '2017-04-22 17:59:38', '2017-04-23 14:31:36', '', null, '人事部', '4');
INSERT INTO `mm_tree_trunk` VALUES ('86', '人事部', '1', 'HM-1', '0', '2', '1', '2017-04-22 18:52:02', '2017-04-22 18:52:02', '', null, '人力资源管理', '1');
INSERT INTO `mm_tree_trunk` VALUES ('87', '国防部', '1', '666', '0', '1', '1', '2017-05-19 22:46:53', '2017-05-30 01:41:37', '', null, null, '4');
INSERT INTO `mm_tree_trunk` VALUES ('88', '吃葡萄不吐葡萄皮', '2', '0022332', '0', '1', '1', '2017-05-21 03:09:23', '2017-05-30 01:41:42', '', null, '不吃葡萄偏吐葡萄皮', '4');
INSERT INTO `mm_tree_trunk` VALUES ('89', '圆珠笔', '2', '000043', '0', '1', '1', '2017-06-02 01:16:00', '2017-06-02 01:16:00', '', null, null, '1');
INSERT INTO `mm_tree_trunk` VALUES ('90', '服务器', '2', '001', '0', '1', '1', '2018-01-05 16:37:25', '2018-01-05 16:37:25', '', null, '服务器', '1');
INSERT INTO `mm_tree_trunk` VALUES ('91', '1U服务器', '2', '002', '90', '1', '1', '2018-01-05 16:38:40', '2018-01-05 16:38:40', '', null, '1U服务器', '1');
INSERT INTO `mm_tree_trunk` VALUES ('92', '324', '2', '12345', '0', '1', '1', '2018-01-10 03:10:36', '2018-01-10 03:10:36', '', null, '1234', '1');
INSERT INTO `mm_tree_trunk` VALUES ('93', '4565', '2', '4567', '0', '1', '1', '2018-01-10 03:10:58', '2018-01-10 03:10:58', '', null, '657', '1');
INSERT INTO `mm_tree_trunk` VALUES ('94', 'asdfasd', '1', 'asdfasdff', '0', '1', '1', '2018-01-10 08:29:10', '2018-01-10 08:29:10', '', null, null, '1');
INSERT INTO `mm_tree_trunk` VALUES ('95', 'ttt', '2', '9999', '0', '1', '1', '2018-01-11 07:09:16', '2018-01-11 07:09:16', '', null, null, '1');
INSERT INTO `mm_tree_trunk` VALUES ('96', 'www', '2', '1112', '95', '1', '1', '2018-01-11 07:09:33', '2018-01-11 07:09:33', '', null, null, '1');
INSERT INTO `mm_tree_trunk` VALUES ('97', 'ddd', '2', '1113', '96', '1', '1', '2018-01-11 07:09:45', '2018-01-11 07:09:45', '', null, null, '1');
INSERT INTO `mm_tree_trunk` VALUES ('98', 'ffff', '2', '1114', '97', '1', '1', '2018-01-11 07:09:54', '2018-01-11 07:09:54', '', null, null, '1');

-- ----------------------------
-- Table structure for mm_using_record
-- ----------------------------
DROP TABLE IF EXISTS `mm_using_record`;
CREATE TABLE `mm_using_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `material_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tree_trunk_id` int(11) NOT NULL,
  `company_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `startTime` timestamp NULL DEFAULT NULL,
  `deadline` timestamp NULL DEFAULT NULL,
  `has_deliver` tinyint(4) NOT NULL DEFAULT '0',
  `delete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mm_using_record
-- ----------------------------
INSERT INTO `mm_using_record` VALUES ('1', '1', '3', '24', '1', 'huiuddw', '2017-03-17 17:24:05', '2017-03-20 09:25:43', '4', '1');
INSERT INTO `mm_using_record` VALUES ('2', '1', '3', '24', '1', 'huiuddw', '2017-03-18 10:16:25', '2017-03-20 09:26:11', '4', '1');
INSERT INTO `mm_using_record` VALUES ('3', '1', '3', '24', '1', 'huiuddw', '2017-03-18 08:00:00', '2017-03-20 09:26:13', '4', '1');
INSERT INTO `mm_using_record` VALUES ('4', '1', '3', '24', '1', 'huiuddw', '2017-03-18 08:00:00', '2017-03-20 09:27:28', '4', '1');
INSERT INTO `mm_using_record` VALUES ('5', '2', '3', '24', '1', 'test----------hhahha', '2017-03-18 08:00:00', '2017-03-20 09:27:31', '4', '1');
INSERT INTO `mm_using_record` VALUES ('6', '1', '3', '24', '1', 'huiuddw', '2017-03-18 08:00:00', '2017-03-20 09:24:26', '4', '1');
INSERT INTO `mm_using_record` VALUES ('7', '8', '3', '24', '1', 'huuu', '2017-03-19 08:00:00', '2017-03-20 09:26:16', '4', '1');
INSERT INTO `mm_using_record` VALUES ('8', '11', '3', '0', '1', 'test', '2017-03-19 20:20:42', '2017-03-20 09:24:35', '4', '1');
INSERT INTO `mm_using_record` VALUES ('9', '11', '3', '0', '1', 'test', '2017-03-19 20:26:51', '2017-03-20 09:27:55', '4', '1');
INSERT INTO `mm_using_record` VALUES ('10', '7', '3', '0', '1', 'uuii', '2017-03-19 20:48:03', '2017-03-20 09:35:25', '4', '1');
INSERT INTO `mm_using_record` VALUES ('11', '9', '3', '0', '1', '12333', '2017-03-20 20:59:10', '2017-03-21 11:44:02', '4', '1');
INSERT INTO `mm_using_record` VALUES ('12', '11', '3', '0', '1', 'test', '2017-03-20 09:06:42', '2017-03-21 11:44:05', '4', '1');
INSERT INTO `mm_using_record` VALUES ('13', '11', '3', '0', '1', 'test', '2017-03-20 10:03:02', '2017-03-21 11:44:06', '4', '1');
INSERT INTO `mm_using_record` VALUES ('14', '11', '3', '0', '1', 'test', '2017-03-20 10:07:39', '2017-03-21 11:44:39', '4', '1');
INSERT INTO `mm_using_record` VALUES ('15', '7', '3', '0', '1', 'uuii', '2017-03-20 10:08:27', '2017-03-21 11:44:41', '4', '1');
INSERT INTO `mm_using_record` VALUES ('16', '7', '3', '0', '1', 'uuii', '2017-03-20 10:08:29', '2017-03-29 10:54:48', '4', '1');
INSERT INTO `mm_using_record` VALUES ('17', '11', '3', '0', '1', 'test', '2017-03-20 10:44:10', '2017-03-21 14:19:33', '4', '1');
INSERT INTO `mm_using_record` VALUES ('18', '11', '3', '0', '1', 'test', '2017-03-20 10:46:16', '2017-03-27 17:06:35', '4', '1');
INSERT INTO `mm_using_record` VALUES ('19', '11', '3', '0', '1', 'test', '2017-03-21 14:21:32', '2017-03-27 17:06:36', '4', '1');
INSERT INTO `mm_using_record` VALUES ('20', '7', '3', '0', '1', 'uuii', '2017-03-23 16:47:04', '2017-03-27 17:06:39', '4', '1');
INSERT INTO `mm_using_record` VALUES ('21', '3', '3', '0', '1', 'hiujd', '2017-03-26 15:27:06', '2017-03-27 17:06:40', '4', '1');
INSERT INTO `mm_using_record` VALUES ('22', '2', '3', '0', '1', 'huiuddw', '2017-03-26 16:06:50', '2017-03-29 10:06:30', '4', '1');
INSERT INTO `mm_using_record` VALUES ('24', '1', '3', '0', '1', 'huiuddw', '2017-03-26 17:18:12', '2017-03-29 10:04:38', '4', '1');
INSERT INTO `mm_using_record` VALUES ('25', '2', '3', '0', '1', 'huiuddw', '2017-03-27 10:50:00', '2017-03-29 10:02:50', '4', '1');
INSERT INTO `mm_using_record` VALUES ('26', '2', '3', '0', '1', 'huiuddw', '2017-03-29 10:30:38', '2017-03-29 10:54:41', '4', '1');
INSERT INTO `mm_using_record` VALUES ('27', '11', '3', '0', '1', 'test', '2017-03-29 10:45:01', '2017-03-29 11:12:52', '4', '1');
INSERT INTO `mm_using_record` VALUES ('28', '2', '3', '0', '1', 'huiuddw', '2017-03-29 11:03:56', '2017-03-29 14:57:26', '4', '1');
INSERT INTO `mm_using_record` VALUES ('29', '3', '3', '0', '1', 'hiujd', '2017-03-29 11:09:51', '2017-03-29 11:12:55', '4', '4');
INSERT INTO `mm_using_record` VALUES ('30', '7', '3', '0', '1', 'uuii', '2017-03-29 11:10:44', '2017-03-29 11:17:48', '4', '1');
INSERT INTO `mm_using_record` VALUES ('31', '3', '3', '0', '1', 'hiujd', '2017-03-29 11:13:06', '2017-03-29 11:17:50', '4', '1');
INSERT INTO `mm_using_record` VALUES ('32', '11', '3', '0', '1', 'test', '2017-03-29 11:17:18', '2017-03-29 11:17:51', '4', '1');
INSERT INTO `mm_using_record` VALUES ('33', '3', '3', '0', '1', 'hiujd', '2017-03-29 11:18:02', '2017-03-29 11:48:15', '4', '1');
INSERT INTO `mm_using_record` VALUES ('34', '11', '3', '0', '1', 'test', '2017-03-29 11:21:22', '2017-03-29 11:48:18', '4', '1');
INSERT INTO `mm_using_record` VALUES ('35', '11', '3', '0', '1', 'test', '2017-03-29 14:42:23', '2017-04-08 08:00:00', '4', '1');
INSERT INTO `mm_using_record` VALUES ('36', '2', '3', '0', '1', 'huiuddw', '2017-03-29 15:05:24', '2017-04-08 08:00:00', '4', '1');
INSERT INTO `mm_using_record` VALUES ('37', '7', '3', '0', '1', 'uuii', '2017-04-10 11:04:46', '2017-04-11 09:19:36', '4', '1');
INSERT INTO `mm_using_record` VALUES ('38', '7', '3', '0', '1', 'uuii', '2017-04-11 09:18:16', '2017-04-11 09:19:38', '4', '1');
INSERT INTO `mm_using_record` VALUES ('39', '7', '3', '0', '1', 'uuii', '2017-04-11 09:19:54', '2017-04-11 09:25:53', '4', '1');
INSERT INTO `mm_using_record` VALUES ('40', '3', '3', '0', '1', 'hiujd', '2017-04-11 09:22:18', '2017-04-11 09:25:55', '4', '1');
INSERT INTO `mm_using_record` VALUES ('41', '3', '3', '0', '1', 'hiujd', '2017-04-11 09:26:07', '2017-04-11 11:03:53', '4', '1');
INSERT INTO `mm_using_record` VALUES ('42', '7', '3', '0', '1', 'uuii', '2017-04-11 09:26:20', '2017-04-11 11:03:55', '4', '1');
INSERT INTO `mm_using_record` VALUES ('43', '3', '3', '0', '1', 'hiujd', '2017-04-11 11:04:14', '2017-04-11 11:11:52', '4', '1');
INSERT INTO `mm_using_record` VALUES ('44', '7', '3', '0', '1', 'uuii', '2017-04-11 11:05:31', '2017-04-11 11:11:57', '4', '1');
INSERT INTO `mm_using_record` VALUES ('45', '7', '3', '0', '1', 'uuii', '2017-04-11 11:12:11', '2017-04-11 11:28:06', '4', '1');
INSERT INTO `mm_using_record` VALUES ('46', '3', '3', '0', '1', 'hiujd', '2017-04-11 11:12:23', '2017-04-11 11:28:07', '4', '1');
INSERT INTO `mm_using_record` VALUES ('47', '1', '3', '0', '1', 'huiuddw', '2017-04-11 11:12:37', '2017-04-11 11:28:08', '4', '1');
INSERT INTO `mm_using_record` VALUES ('48', '3', '3', '0', '1', 'hiujd', '2017-04-11 11:28:21', '2017-04-11 11:34:44', '4', '1');
INSERT INTO `mm_using_record` VALUES ('49', '7', '3', '0', '1', 'uuii', '2017-04-11 11:28:33', '2017-04-11 11:34:45', '4', '1');
INSERT INTO `mm_using_record` VALUES ('50', '1', '3', '0', '1', 'huiuddw', '2017-04-11 11:28:46', '2017-04-11 11:34:46', '4', '1');
INSERT INTO `mm_using_record` VALUES ('51', '12', '3', '0', '1', '1234', '2017-04-11 11:29:01', '2017-04-11 11:34:47', '4', '1');
INSERT INTO `mm_using_record` VALUES ('52', '13', '3', '0', '1', '软件测试类书籍', '2017-04-11 11:29:18', '2017-04-11 11:34:48', '4', '1');
INSERT INTO `mm_using_record` VALUES ('53', '3', '3', '0', '1', 'hiujd', '2017-04-11 11:34:58', '2017-04-11 14:55:49', '4', '1');
INSERT INTO `mm_using_record` VALUES ('54', '12', '3', '0', '1', '1234', '2017-04-11 11:35:11', '2017-04-11 14:51:00', '4', '1');
INSERT INTO `mm_using_record` VALUES ('55', '13', '3', '0', '1', '软件测试类书籍', '2017-04-11 11:35:28', '2017-04-11 14:53:27', '4', '1');
INSERT INTO `mm_using_record` VALUES ('56', '7', '3', '0', '1', 'uuii', '2017-04-11 11:35:41', '2017-04-11 14:57:19', '4', '1');
INSERT INTO `mm_using_record` VALUES ('57', '1', '3', '0', '1', 'huiuddw', '2017-04-11 11:35:58', '2017-04-11 15:02:09', '4', '1');
INSERT INTO `mm_using_record` VALUES ('58', '7', '3', '0', '1', 'uuii', '2017-04-11 15:00:14', '2017-04-11 15:26:40', '4', '1');
INSERT INTO `mm_using_record` VALUES ('59', '3', '3', '0', '1', 'hiujd', '2017-04-11 15:00:44', '2017-04-11 15:07:34', '4', '1');
INSERT INTO `mm_using_record` VALUES ('60', '13', '3', '0', '1', '软件测试类书籍', '2017-04-11 15:00:57', '2017-04-11 15:03:04', '4', '1');
INSERT INTO `mm_using_record` VALUES ('61', '12', '3', '0', '1', '1234', '2017-04-11 15:01:14', '2017-04-11 15:02:35', '4', '1');
INSERT INTO `mm_using_record` VALUES ('62', '3', '3', '0', '1', 'hiujd', '2017-04-11 15:08:28', '2017-04-11 15:30:13', '4', '1');
INSERT INTO `mm_using_record` VALUES ('63', '1', '3', '0', '1', 'huiuddw', '2017-04-11 15:25:29', '2017-04-11 15:31:33', '4', '1');
INSERT INTO `mm_using_record` VALUES ('64', '13', '3', '0', '1', '软件测试类书籍', '2017-04-11 15:25:42', '2017-04-11 15:35:21', '4', '1');
INSERT INTO `mm_using_record` VALUES ('65', '12', '3', '0', '1', '1234', '2017-04-11 15:25:56', '2017-04-11 15:27:11', '4', '1');
INSERT INTO `mm_using_record` VALUES ('66', '12', '3', '0', '1', '1234', '2017-04-11 15:27:57', '2017-04-13 10:04:10', '4', '1');
INSERT INTO `mm_using_record` VALUES ('67', '7', '3', '0', '1', 'uuii', '2017-04-11 15:28:29', '2017-04-17 10:16:51', '4', '1');
INSERT INTO `mm_using_record` VALUES ('68', '7', '3', '0', '1', 'uuii', '2017-04-13 10:07:37', '2017-04-17 10:16:09', '4', '1');
INSERT INTO `mm_using_record` VALUES ('69', '3', '3', '0', '1', '', '2017-04-13 10:52:02', '2017-04-23 08:00:00', '3', '4');
INSERT INTO `mm_using_record` VALUES ('70', '1', '3', '0', '1', '', '2017-04-13 15:21:22', '2017-04-17 10:13:55', '4', '1');
INSERT INTO `mm_using_record` VALUES ('71', '7', '3', '0', '1', '', '2017-04-16 09:52:48', '2017-04-17 10:11:44', '4', '2');
INSERT INTO `mm_using_record` VALUES ('72', '7', '3', '0', '1', '', '2017-04-17 09:06:19', '2017-04-18 09:05:16', '4', '4');
INSERT INTO `mm_using_record` VALUES ('73', '3', '38', '0', '1', '', '2017-04-17 11:12:38', '2017-04-27 08:00:00', '3', '4');
INSERT INTO `mm_using_record` VALUES ('74', '1', '38', '0', '1', '', '2017-04-17 11:14:42', '2017-04-27 08:00:00', '2', '4');
INSERT INTO `mm_using_record` VALUES ('76', '7', '38', '0', '1', '', '2017-04-19 11:41:19', '2017-04-29 08:00:00', '2', '4');
INSERT INTO `mm_using_record` VALUES ('77', '1', '3', '0', '1', '', '2017-04-20 10:36:40', '2017-04-20 10:39:01', '4', '1');
INSERT INTO `mm_using_record` VALUES ('78', '1', '3', '0', '1', '', '2017-04-20 10:40:24', '2017-04-20 10:40:55', '4', '1');
INSERT INTO `mm_using_record` VALUES ('79', '15', '37', '41', '1', '', '2017-04-20 10:44:47', '2017-04-20 10:45:34', '4', '2');
INSERT INTO `mm_using_record` VALUES ('80', '15', '3', '0', '1', '', '2017-04-20 09:59:32', '2017-04-20 09:59:54', '4', '1');
INSERT INTO `mm_using_record` VALUES ('81', '16', '3', '0', '1', '', '2017-04-21 16:22:05', '2017-04-21 09:43:14', '4', '1');
INSERT INTO `mm_using_record` VALUES ('82', '17', '38', '0', '1', '', '2017-04-21 16:24:51', '2017-04-21 09:00:04', '4', '1');
INSERT INTO `mm_using_record` VALUES ('83', '18', '38', '0', '1', '', '2017-04-21 17:50:39', '2017-04-21 20:59:50', '4', '4');
INSERT INTO `mm_using_record` VALUES ('84', '17', '3', '0', '1', '', '2017-04-21 09:14:45', '2017-05-01 08:00:00', '3', '4');
INSERT INTO `mm_using_record` VALUES ('85', '19', '38', '0', '1', '', '2017-04-21 09:40:08', '2017-04-21 09:42:08', '4', '1');
INSERT INTO `mm_using_record` VALUES ('86', '16', '38', '0', '1', '', '2017-04-22 10:40:52', '2017-04-23 13:12:35', '4', '1');
INSERT INTO `mm_using_record` VALUES ('87', '7', '3', '0', '1', '', '2017-04-22 11:35:53', '2017-04-23 10:02:21', '4', '1');
INSERT INTO `mm_using_record` VALUES ('88', '7', '3', '0', '1', '', '2017-04-23 10:58:38', '2017-04-23 10:59:50', '4', '1');
INSERT INTO `mm_using_record` VALUES ('89', '16', '38', '0', '1', '', '2017-04-23 13:13:54', '2017-05-10 03:53:12', '4', '1');
INSERT INTO `mm_using_record` VALUES ('90', '15', '43', '0', '2', '', '2017-05-06 01:20:03', '2017-05-06 01:20:08', '4', '1');
INSERT INTO `mm_using_record` VALUES ('91', '15', '43', '0', '2', '', '2017-05-06 01:20:31', '2017-06-04 16:00:00', '1', '1');
INSERT INTO `mm_using_record` VALUES ('92', '16', '3', '0', '1', '', '2017-05-09 20:29:31', '2017-05-09 20:29:38', '4', '1');
INSERT INTO `mm_using_record` VALUES ('93', '21', '37', '41', '1', '', '2017-05-24 18:43:46', '2017-06-23 16:00:00', '0', '1');
INSERT INTO `mm_using_record` VALUES ('94', '20', '43', '0', '2', '', '2017-05-24 23:21:10', '2017-06-23 16:00:00', '1', '1');
INSERT INTO `mm_using_record` VALUES ('95', '18', '3', '0', '1', '', '2017-05-24 17:44:39', '2017-06-23 16:00:00', '2', '1');
INSERT INTO `mm_using_record` VALUES ('96', '16', '38', '0', '1', '', '2017-06-05 18:23:17', '2017-06-05 18:23:46', '4', '1');
INSERT INTO `mm_using_record` VALUES ('97', '3', '38', '0', '1', '', '2017-09-21 19:29:09', '2017-09-21 19:30:30', '4', '1');
INSERT INTO `mm_using_record` VALUES ('98', '21', '3', '0', '1', '', '2017-12-30 02:12:27', '2018-01-03 07:08:37', '4', '1');
INSERT INTO `mm_using_record` VALUES ('99', '16', '37', '41', '1', '', '2018-01-03 07:11:17', '2018-01-05 04:33:57', '4', '1');
INSERT INTO `mm_using_record` VALUES ('100', '22', '3', '0', '1', '', '2018-01-05 04:39:47', '2018-01-05 04:41:29', '4', '1');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('1373918920@qq.com', '$2y$10$E99s2fEWf9/l2rEMGDlQvuVvC4s0.hoaEILeMcoABdUaktec8a9iC', '2017-03-09 15:26:45');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_type` smallint(6) NOT NULL DEFAULT '2' COMMENT '1 为 company_id , tree_trunk_id 所决定节点的管理者，2 为普通的用户，只能查询',
  `tree_trunk_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort` mediumint(9) NOT NULL DEFAULT '0' COMMENT '用于 jstree 树排序',
  `number` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '员工编号',
  `phone` char(11) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `delete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2', 'uchiyou4', '1373918920@qq.com', '$2y$10$c94iqfETk7U6yfWhV3hE2O3YEKq/jiv/XMNm0/oumtRCrceFaVqau', '1', '24', '1', 'dcGFkV922a24jAV4aYnQh500iNIygYppMuQtiCQZrCagDVVzVrJVmMRdU2WU', '2017-03-01 11:48:48', '2017-03-18 20:58:36', '14', 'uchiyou4', '17378118015', '陕西省-西安市-长安区-西安电子科技大学-南校区', '1');
INSERT INTO `users` VALUES ('3', '小游', 'uchiyou@sina.com', '$2y$10$lnZLKNYhZsPtxfpQW3OWheTI7KhaRXXvDR/x.9wLsr9MXotuwwbGm', '1', '0', '1', 'mdzWc0oxiAorHHwRWYlyM900oQse2o8TgxjO834q1pRI4WRfr6DX4zMyAIRa', '2017-03-08 19:48:04', '2017-09-24 22:09:41', '0', '赵信', '17378118015', '陕西省-西安市-高陵区-西安电子科技大学', '1');
INSERT INTO `users` VALUES ('6', 'lisi3', 'lisi@sina.com', '$2y$10$AzaA9LCbZD5m3UwJS02i4uO2Sux2AnCDaqVGSKPP0h9VMejf8zJFO', '1', '26', '1', null, '2017-03-12 11:08:54', '2017-05-30 01:41:32', '6', 'uuu', '17378118015', '陕西省-西安市-长安区-西安电子科技大学-南校区', '4');
INSERT INTO `users` VALUES ('7', 'wangwu3', 'wangwu@sina.com', '$2y$10$L40O9HSRCiG/mqsG./rB8OttMAfHUY5as7dZlubZqjsvIdg2dOqge', '1', '24', '1', null, '2017-03-13 10:25:40', '2017-03-18 20:58:36', '15', 'wang1234', '17378118015', '陕西省-西安市-长安区-西安电子科技大学-南校区', '1');
INSERT INTO `users` VALUES ('8', 'uuu', 'uu@sina.com', '$2y$10$nD2iThwdmQRo3gZXZuyaHOlJLGx.LRHIwLC/8jZgjD6ChNmHUlWX2', '1', '1', '1', 'GjkMc45rICK6NsLVDphrCr0vZhpLALGYV2P1GDaow7t9m1V9qTQXD85g8qtL', '2017-04-05 15:02:21', '2017-04-05 15:02:21', '0', null, '', '', '1');
INSERT INTO `users` VALUES ('9', 'zhaoliu', 'zhaoliu@sina.com', '$2y$10$oouK0qRdxE9zNt4d6ziNSOEZ/gT52BxJPlo/6kAyZkQV4142Y61Re', '1', '1', '1', 'vxH7tRZAh7s8R9y32vCBNMmP6vxA1GVjAQYTdPKNpprerXiqAONfLVs3GAOG', '2017-04-06 09:21:44', '2017-04-06 09:21:44', '0', null, '', '', '1');
INSERT INTO `users` VALUES ('10', '赵信', 'zhaoxing@qq.com', '$2y$10$Ns61ZJajKDiofZ4T8YGobON8kBkxleILOvHvPZvx.Rv1zk16rE4Iu', '1', '1', '1', '7u4q9HwOu14vRXiQqP6gsFgkPho4sxAMZZmEECz9IozuO5HQipKsCdLd1NCj', '2017-04-06 09:35:31', '2017-04-06 09:35:31', '0', null, '', '', '1');
INSERT INTO `users` VALUES ('11', '盖伦', 'gaylun@sina.com', '$2y$10$7VU0Nsgdb.MQUlyBfIFe7Owzey0dqTlcvCC81iBU./2ogNwbpSz6i', '1', '1', '1', 'SGcDz0CMrqMRdXYy6pKWwldBzV01faZdnHO5fzJFNCAu57RWTE8BMySHN8NI', '2017-04-06 09:40:41', '2017-04-06 09:40:41', '0', null, '', '', '1');
INSERT INTO `users` VALUES ('12', '莫甘娜', 'moganna@sina.com', '$2y$10$GCz.sBKRD/D45m3XnlDkAu9BgRgeoWrGuPneICURJCAlcN9n5QP9S', '1', '1', '1', 'D3fIjV3FUQPpjDdGBst9m0wBtqyu5b0eXuPrurkQdxjPVucN4BZxEUPcUnU4', '2017-04-06 09:50:54', '2017-04-06 09:50:54', '0', null, '', '', '1');
INSERT INTO `users` VALUES ('13', '阿木木', 'mumu@sina.com', '$2y$10$MrK8BJ2QubhDcL78mQpuaeqe/6.9d.4U8eMlyMu/RuKrmadHYMWzq', '1', '1', '1', 'OaPAm5QxIdxmbrnHbm1rmqNSX4KSRctOrI1DEnhCACAb5mw1iDq7SOXU9Pdw', '2017-04-06 09:59:53', '2017-04-06 09:59:53', '0', null, '', '', '1');
INSERT INTO `users` VALUES ('14', '提莫', 'timo@sina.com', '$2y$10$aCjrrGim8Ppo4q4p02td4ezWa4C6ilAEmTDu2yzxpj2jEw8Uldqai', '1', '1', '1', 'FCjRmLX3Wwhyx38aVnjLy8pG6O2yRmFSzMrIZBX0fPCb84qSMVdFwvI47GHG', '2017-04-06 11:35:16', '2017-04-06 11:35:16', '0', null, '', '', '1');
INSERT INTO `users` VALUES ('15', '熔岩巨兽', 'rongyan@sina.com', '$2y$10$9.NNkiPejrSF7V4/6vzb2OX8N39zNS5qLJtrr3GDAtwmaxS6MewVi', '1', '1', '1', '6utvIFlDcuJ3mqOCfy6f77VQGwHTQgXoJkDI3nDTxowahjo9i0ACMytCy1oU', '2017-04-06 11:37:52', '2017-04-06 11:37:52', '0', null, '', '', '1');
INSERT INTO `users` VALUES ('16', '无极剑圣', 'jianshen@sina.com', '$2y$10$hQ8ULn5/aBFmqrvq6pvxAeeq0Kdnk9aGE4UniENpcAGfZXYeuu7OW', '1', '1', '1', 'V1nyOdZ5jv4CZvlcmFHXqS9MIbfSXdcQmDUbuIttxJwfFPmKfBNvZX2uTPTv', '2017-04-06 11:40:35', '2017-04-06 11:40:35', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('17', '堕落天使', 'duoluotianshi@sina.com', '$2y$10$jWGvFI6eDJwfvhxhe1UIO.tFmMM27eQ9EBUubaXl0lJnWDfzjV/6S', '1', '1', '1', 'bfbgkmSPFipc9p7JRQegMUFYkMxzQ0BXx3zATj0v68Zp44gC3Q4MCWsJLNg5', '2017-04-07 10:36:51', '2017-04-07 10:36:51', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('18', '疾风剑豪', 'jifenjianhao@sina.com', '$2y$10$34jnWrXorzC6GdMXxot2d.pkVbovUE5bknJW45EMOZZD4PC/jAWry', '1', '1', '1', '9eYhuYSE6YlWcxDIyIEZRewMOs0KRFMRkQ7schphX5x2YSB14FbLbAPNSyHW', '2017-04-07 10:40:53', '2017-04-07 10:40:53', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('19', '亚索', 'yasuou@sina.com', '$2y$10$5w0HmuAt.GzOmmcagEC74.bl785PjeVI0cxYZZoLlS0xPGDQWCwXS', '1', '1', '1', null, '2017-04-07 10:45:04', '2017-04-07 10:45:04', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('21', '周杰伦', 'jchou@sina.com', '$2y$10$BRCa7a0eGqJeHyJECUPPxOW/At4jiVMczm/3tYUw5WE/kd.7884V6', '1', '1', '1', 'm8LhcpSLBgZIi7BwPpdXJ9RyavC3FHEQeS4Pj41aUyXpj04cCExNoOl6O1wm', '2017-04-07 21:10:45', '2017-04-07 21:10:45', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('22', '小人鱼', 'renyu@sina.com', '$2y$10$oknmdRNp0nYydY9A/0TG1ueT2E7KlaPjIWCXkBqz9E8cab526sn66', '1', '1', '1', 'wJYMEEIWJmnW7cOsqR89otiVjoudwfxBDOGG7hc9QIhL9Ginkmjesqkr5gCq', '2017-04-07 21:19:16', '2017-04-07 21:19:16', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('23', '冯提莫', 'fengtimo@sina.com', '$2y$10$U87rCJtdoMTKqnDZ1n2xAOthx/kCWeWqjW4QQaGiI90m4E4Wqr7vO', '1', '1', '1', 'BwKQxGdiW3oX5qe89IOlTIhIpye8i9fCiXJNdRy8317qbxKNXhot1unRtOQr', '2017-04-07 21:26:02', '2017-04-07 21:26:02', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('24', '麦林小、', 'xiaopaoshou@qq.com', '$2y$10$GpBh1FlkmFgImbGB/cqTWOIt3tdc1vKvKI1LEkAkAeg3LSr.IciiS', '1', '1', '1', 'xhqGLoHmVVjKATgC0e1swR4aN64FGqKW8W06iPQIxvNYl3z3qEyDGcYaFSBk', '2017-04-09 09:18:12', '2017-04-09 09:18:12', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('25', '蒙多医生', 'menduo@sina.com', '$2y$10$eHt.e8A583wH6BUgFu6cGOe42101f7KRKI9lDC9KaHgtCuTQOPrXO', '1', '1', '1', 'dgcdy042jiyf6vCPonTOZgV7J9tlC7mgRANLjLjUhCpOgYU2ZuO6pPl0TFrk', '2017-04-09 09:34:47', '2017-04-09 09:34:47', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('26', '皮城女警', 'picenglvjin@sina.com', '$2y$10$pvJerpgLargVYISFiLw7iulmMNF3kCUaGvkrgmSifV.8vgB7C/i/W', '1', '1', '1', '6PaXX5wJtwKz3YlNvjDn8XAIXG4RG33ttsFsD16xdZl4g2l0krPP1JcpaL9q', '2017-04-11 09:31:32', '2017-04-11 09:31:32', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('27', '光辉女郎', 'guanghui@sina.com', '$2y$10$epM1kGgVD.sDwBBfKEGU3uITojahhIvrNPP80OmloI/emklKjJ.Cu', '1', '1', '1', 'fS3YXheVnzvxdTzftSgq30sOChdReIA0hfqyTS5dzh9TSDOESkADzeyg3H1t', '2017-04-11 09:39:33', '2017-04-11 09:39:33', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('28', '机器人', 'robot@sina.com', '$2y$10$Pj6ET5cr0WCfL3Dr1AqVBuyJQNN/LOyM.QTJVr4TIPRW2Zd/ru1a2', '1', '1', '1', 'pIzSB01I9yJPzjD9pQKXE4u54sxTwgsOT0C5YBqOMrwWsODpxVlzTnjTcOjh', '2017-04-11 10:01:33', '2017-04-11 10:01:33', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('29', '恶灵典狱长', 'elingdianyuzhang@sina.com', '$2y$10$YJNJTuWku0WpPl4Ld8mghO2x6ae2Oh0VWFMqA4sc/TXnhnqT68f4O', '1', '1', '1', 'RP9JuZ24oBx8x5tvaye4erv8Ntj9aa7XvexkT7nkGUaubMjMajN2J7nW4Unl', '2017-04-11 10:05:06', '2017-04-11 10:05:06', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('30', '恶灵骑士', 'elingqingshi@sina.com', '$2y$10$c8OZazt9pjHxWmLeOpV95.ZZ.4lSGS/cF1nmIRxA4bBmhUGk2CAjO', '1', '1', '1', 'eUwprzpgEufY0R0UNBnqcE0Sux89J056tcmy2fNCYLPAWlsZZivYft6fMgW5', '2017-04-11 10:32:27', '2017-04-11 10:32:27', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('31', '迅捷斥候', 'xujiechihou@sina.com', '$2y$10$08izooo1byGjVuWkaae97uuqcokGWgGUABoylcFYXKYy6XTUBJl4e', '1', '1', '1', 'T0hUQJNf73AWDhbjxkb57AufMsp5x55g12Sq8FGFo57BDKeZNwSendxlYtTC', '2017-04-11 10:36:09', '2017-04-11 10:36:09', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('32', '加里奥', 'jialiao@sina.com', '$2y$10$U8l622TwtzZyYvrhfskOI.646NJR.ylMk0qI1gOre0jzTrLd9vTHG', '1', '0', '1', 'sNAOp55NQSj0xgXimdi0eblZPBKSKAvsUHAL9IQFpNl8yutKjd4Nz99WKWdu', '2017-04-11 10:43:08', '2017-04-11 10:43:08', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('33', '瘟疫之源', 'wenyizhiyuan@sina.com', '$2y$10$IMyIYqHOLprISYmwyjEgKObwMCrTQ0X9fmClhI2NINI0wm6X2fLOq', '1', '1', '1', 'mal8e1HnpFTRTyWItqxPOzsTB1Xswfy6B9VTHZV4hXaLbWMLYI3VTWmWQi9N', '2017-04-11 10:46:33', '2017-04-11 10:46:33', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('34', '末日使者', 'morishizhe@sina.com', '$2y$10$yahpsUcKC7/PdyYNR9EW6evSNmYM49rDcPFLtkORx.O3LF4v/jXl6', '1', '1', '1', 'FpBh3y3uzNcoe2s8L3GVExlExZtN295EHe4xDVbJWgX7uAWIoGWuOgQ5pls6', '2017-04-11 10:47:35', '2017-04-11 10:47:35', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('35', '稻草人', 'daocaoren@sina.com', '$2y$10$ZURPhCwTyHXSdTaERHKGseNebCD/RoBT/eFjr6ldJSu6feWRY50C6', '1', '1', '1', 'Pujnvdw21E21LRVX2EIgqWXZdVriz4pmTZqifgJWrGrFHfwbKYABJziEZa1L', '2017-04-11 10:48:46', '2017-04-11 10:48:46', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('36', '沼泽魔蛙', 'zhaozhemowa@sina.com', '$2y$10$fHR9JoiEmbGCK.NNKLQl9.kCMcAtbv.5Tw1T7w7nLJR37hMbL25eK', '2', '56', '1', null, '2017-04-13 15:02:23', '2017-04-20 10:12:10', '0', '13130110075', '', '', '4');
INSERT INTO `users` VALUES ('37', 'nash', 'nash@sina.com', '$2y$10$aSAIORyB/aThAjzUhDwAfePbqSvtqKiGMKYEApi9GTmNedH7GOOIy', '1', '41', '1', 'X7njIaTeoh9hb4P9N3ZypemH313b0Ymd3fcdv4sGg8MoOpMl43tumeHAqtjm', '2017-04-13 15:16:15', '2017-04-20 10:48:01', '0', '13130110076', '18778118015', '湖南省-长沙市-芙蓉区-长沙理工大学', '1');
INSERT INTO `users` VALUES ('38', '阿狸', 'ali@sina.com', '$2y$10$zKQ7Va30QxjLtE.miqsZZefTVTfDWxF9u/ieDZYtxnXTbs/3m6AmG', '2', '0', '1', 'kiSnT2R7slFoVgzZ3JHNsAHEUm9xRMuxvbv9ePl1ppHaLheT7KIwt95r5vtx', '2017-04-13 15:19:21', '2017-05-11 08:38:20', '10', '13130110077', '17378118015', '陕西省-西安市-长安区-西安电子科技大学', '1');
INSERT INTO `users` VALUES ('39', '纳什男爵', 'nashinanjue@sina.com', '$2y$10$5DvuR.7Haml/3f0LaEgtK.umwBUBJmR.0E6J8gbj.WJx6aoH7xQVq', '1', '44', '1', null, '2017-04-20 09:41:23', '2017-04-20 10:11:24', '0', 'M-183', '', '', '4');
INSERT INTO `users` VALUES ('40', '潘玮柏', 'panweibo@sina.com', '$2y$10$whXicqPmfb05FeVvCSvJLeyEFuee4ZOhOcmDmngQY0QqGfCsBgYbe', '1', '1', '1', 'pERlxSO1fFEqyxXw7rf0qNEdg26cNSRpfgjwxaCLQSnUrSA5NH2y9jJzx0IS', '2017-04-22 17:23:26', '2017-04-22 17:23:26', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('41', '林俊杰', 'lingjunjie@sina.com', '$2y$10$dRjQ7EvoLSfd/Xg.36t9O.eVbZf1Za.XUUxnuGQbGgzLliFvs8xhi', '1', '1', '1', 'eg4iyQpdiTl2eGDPZljd2h1VTyCP1YZcnKyIjQrLz4CQQazmnWSd4VEpkTmG', '2017-04-22 17:28:58', '2017-04-22 17:28:58', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('42', '李超人', 'licao@sina.com', '$2y$10$BztaDqkY5SNVu8ucdUfflebcF/G45JcFLlpP9nzAabU9nKTiKwJH2', '1', '0', '1', 'MHrXTpcZuUqHr9cWIlCFoS9vOVDrErZup2kelaPs063LNB1tao3USDP1X6Kz', '2017-04-22 17:57:54', '2017-12-25 16:00:24', '0', null, '17378118015', '', '1');
INSERT INTO `users` VALUES ('43', '孙俪', 'suli@sina.com', '$2y$10$betk7dwKh/jSA5BrbkTwgugrbCL3KFiNQ4rW5x1hNJXsO4bFn0yhK', '4', '0', '2', 'JbNNkvl1lUYGk6ArWW7vtUcaQDyfqNa4HD9Cf5XFEGVabJ3pN3qVW3ldpJhs', '2017-04-22 18:51:09', '2017-05-24 23:20:56', '0', null, '17378118015', '北京市-北京市市辖区-东城区-西安电子科技大学', '1');
SET FOREIGN_KEY_CHECKS=1;
